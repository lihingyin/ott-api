<?php

use App\Http\Controllers\APIAuth\APIAuthController;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\EpisodeController;
use App\Http\Controllers\StreamingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['accept.json', 'after']], function () {
    // 1.1 Device Register
    Route::post('device/register',[DeviceController::class, 'registerDevice'])->name('registerDevice');

    // 2.1  Login
    Route::post('auth/login', [APIAuthController::class, 'login'])->name('userLogin');

    // 2.2  User Registration
    Route::post('auth/register', [APIAuthController::class, 'register'])->name('userRegister');

    Route::middleware(['auth:sanctum'])->group(function () {
        // 2.3  Get User Info
        Route::get('auth/user', [APIAuthController::class, 'info'])->name('userInfo');
        // 2.4 Logout
        Route::post('auth/logout', [APIAuthController::class, 'logout'])->name('userLogout');

        // 3.1 Channel List
        Route::get('channel', [ChannelController::class, 'list'])->name('channelList');

        // 4.1 Episode List
        Route::get('channel/{channelId}/episode', [EpisodeController::class, 'list'])->name('channelList');

        // 5.1 Get Streaming Info - Live
        Route::get('channel/{channelId}/streaming-info', [StreamingController::class, 'getLiveInfo'])->name('getLiveStreamingInfo');
        // 5.2 Get Streaming Info - Linear
        Route::get('channel/{channelId}/episode/{episodeId}/streaming-info', [StreamingController::class, 'getLinearInfo'])->name('getLinearStreamingInfo');
    });

});
