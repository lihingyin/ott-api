# OTT 

### Requirement
- PHP 8.1+

## Project Setup guide
_Run the following commands using the command line_

### Setup .env file
- Go to Project directory
    - run `cd {project-directory}`
    - _(Example of `{project-directory}`: `~/development/repo/ott-api`)_
- run `cp -a .env.example .env`
- update `BUGSNAG_API_KEY` for enable error monitoring if wanted
- set `ACCESS_LOG_IN_DATABASE_ENABLE` to be `true` for enable access log storing in database
- set `ACCESS_LOG_IN_STDOUT_ENABLE` to be `true` for enable access log storing in stdout

### Run Composer Install with Docker
- run `docker run -it --rm --name composer2 -d -t -v {project-directory}:/home/source-code composer:2.0.12 composer install -d /home/source-code --ignore-platform-reqs`
- _The docker container `composer2` would be terminated after Composer Install is finished._
- _Please move on to the next step after it is finished._

### Setup Docker
- run `cd {project-directory}/docker`
- run `docker-compose -p ott-api up`

### Generate application key & setup Database
- run `docker exec -it ott-api-app sh -c "php artisan key:generate;php artisan migrate --seed;"`

### Health Check cURL
   ```
   curl --location --request GET 'http://localhost/health'
   ```

### Steps to call authentication required APIs
1. Register Device API
2. Register User account with the registered `uuid` and acquire `token`
3. Use the `token` as Bearer Token to call authentication required APIs
4. You may also acquire `token` with Testing account below

### Testing account
- username : `demo-user`
- password : `md5ed_string`
- uuid : `aaaaaaaa-1111-aaaa-1111-aaaaaaaaaaaa`

### Postman Collection Snapshot
- https://www.postman.com/collections/19e85d15d0261bf7bb9e