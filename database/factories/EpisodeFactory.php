<?php

namespace Database\Factories;

use App\Enums\ChannelType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EpisodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'channel_id' => $this->faker->numberBetween(1, 100),
            'name' => $this->faker->word(),
            'description' => $this->faker->paragraph,
            'thumbnail' => Str::random(40),
            'number' => $this->faker->numberBetween(1, 100),
            'video_id' => Str::random(40),
            'active' => $this->faker->boolean(80),
        ];
    }
}
