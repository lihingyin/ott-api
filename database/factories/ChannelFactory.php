<?php

namespace Database\Factories;

use App\Enums\ChannelType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ChannelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'description' => $this->faker->paragraph,
            'type' => $this->faker->randomElement(array_column(ChannelType::cases(), 'value')),
            'thumbnail' => Str::random(40),
            'active' => $this->faker->boolean(80),
        ];
    }
}
