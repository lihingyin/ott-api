<?php

namespace Database\Factories;

use App\Enums\DeliveryType;
use Illuminate\Database\Eloquent\Factories\Factory;

class LiveStreamingInfoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'channel_id' => $this->faker->numberBetween(1, 100),
            'streaming_url' => $this->faker->url(),
            'delivery_type' => $this->faker->randomElement(array_column(DeliveryType::cases(), 'value')),
        ];
    }
}
