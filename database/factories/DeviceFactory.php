<?php

namespace Database\Factories;

use App\Enums\DevicePlatform;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeviceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => $this->faker->uuid(),
            'platform' => $this->faker->randomElement(array_column(DevicePlatform::cases(), 'value')),
            'primary_device_id' => $this->faker->text(50),
            'os_language' => $this->faker->text(5),
            'app_language' => $this->faker->text(5),
            'os_version' => $this->faker->text(10),
            'app_version' => $this->faker->text(10),
            'build_version' => $this->faker->text(10),
            'app_bundle_id' => $this->faker->text(50),
            'device_name' => $this->faker->word(),
            'device_brand' => $this->faker->word(),
            'device_model' => $this->faker->word(),
            'carrier_name' => $this->faker->word(),
            'fcm_token' => $this->faker->word(),
        ];
    }
}
