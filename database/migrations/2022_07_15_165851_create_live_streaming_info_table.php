<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLiveStreamingInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_streaming_info', function (Blueprint $table) {
            $table->id();
            $table->integer('channel_id');
            $table->string('streaming_url');
            $table->string('delivery_type', 20);
            $table->timestamps();
            $table->softDeletes();
            $table->index('channel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_streaming_info');
    }
}
