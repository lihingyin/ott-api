<?php

namespace Database\Seeders;

use App\Enums\ChannelType;
use App\Models\Channel;
use App\Models\Device;
use App\Models\Episode;
use App\Models\LiveStreamingInfo;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $userName = 'demo-user';
        $defaultUser = User::factory()->create([
            'name' => '陳小明',
            'username' => $userName,
        ]);

        $userId = $defaultUser->id;
        $defaultDevice = Device::factory()->create([
            'uuid' => 'aaaaaaaa-1111-aaaa-1111-aaaaaaaaaaaa',
            'user_id' => $userId,
        ]);

        // Linear Channels
        Channel::factory()
            ->has(
                Episode::factory()
                    ->state(function (array $attributes, Channel $channel) {
                        return [
                            'channel_id' => $channel->id,
                        ];
                    })
                    ->count(24),
                'episodes'
            )
            ->count(18)
            ->create([
                'type' => ChannelType::LINEAR->value,
            ]);

        // Live Channels
        Channel::factory()
            ->has(
                LiveStreamingInfo::factory()
                    ->state(function (array $attributes, Channel $channel) {
                        return [
                            'channel_id' => $channel->id,
                        ];
                    }),
                'liveStreamingInfo'
            )
            ->count(10)
            ->create([
                'type' => ChannelType::LIVE->value,
            ]);
    }
}
