<?php

namespace App\Http\Requests\APIAuth;

use App\Models\Device;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;


class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string', 'max:255'],
            'username' => ['required', 'string', 'unique:' . User::class . ',username'],
            'password' => ['required'],
            'uuid' => ['required', 'string', 'exists:' . Device::class . ',uuid'],
        ];
    }
}
