<?php

namespace App\Http\Requests\Channels;

use App\Enums\ChannelType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChannelListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function prepareForValidation(): void
    {
        $parameters = [];
        if (isset($this->offset) && is_numeric($this->offset)) {
            $parameters['offset'] = (int)$this->offset;
        }
        $this->merge($parameters);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'offset' => ['sometimes', 'required', 'integer'],
            'type' => ['sometimes', 'required',  Rule::in(array_column(ChannelType::cases(), 'value')),],
        ];
    }
}
