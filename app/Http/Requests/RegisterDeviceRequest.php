<?php

namespace App\Http\Requests;

use App\Enums\DevicePlatform;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterDeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation(): void
    {
        $parameters = [];
        if (isset($this->osVersion)) {
            $parameters['osVersion'] = (string)$this->osVersion;
        }
        if (isset($this->appVersion)) {
            $parameters['appVersion'] = (string)$this->appVersion;
        }
        if (isset($this->buildVersion)) {
            $parameters['buildVersion'] = (string)$this->buildVersion;
        }
        if (isset($this->deviceName)) {
            $parameters['deviceName'] = (string)$this->deviceName;
        }
        if (isset($this->deviceBrand)) {
            $parameters['deviceBrand'] = (string)$this->deviceBrand;
        }
        if (isset($this->deviceModel)) {
            $parameters['deviceModel'] = (string)$this->deviceModel;
        }
        if (isset($this->carrierName)) {
            $parameters['carrierName'] = (string)$this->carrierName;
        }
        $this->merge($parameters);
    }


    public function rules()
    {
        $appPlatforms = [
            DevicePlatform::ANDROID->value,
            DevicePlatform::IOS->value,
        ];
        return [
            'uuid' => ['required', 'uuid'],
            'platform' => ['required', Rule::in(array_column(DevicePlatform::cases(), 'value')),],
            'androidId' => ['required_if:platform,' . DevicePlatform::ANDROID->value, 'string', 'max:50'],
            'primaryUUID' => ['required_if:platform,' . DevicePlatform::IOS->value, 'string', 'max:50'],
            'osLanguage' => ['required', 'string', 'max:5'],
            'appLanguage' => ['required', 'string', 'max:5'],
            'osVersion' => ['required_if:platform,' . implode(',', $appPlatforms), 'string', 'max:20'],
            'appVersion' => ['required_if:platform,' . implode(',', $appPlatforms), 'string', 'max:50'],
            'buildVersion' => ['required_if:platform,' . implode(',', $appPlatforms), 'string', 'max:10'],
            'appBundleId' => ['required_if:platform,' . implode(',', $appPlatforms), 'string', 'max:50'],
            'deviceName' => ['sometimes', 'string', 'max:255'],
            'deviceBrand' => ['sometimes', 'string', 'max:255'],
            'deviceModel' => ['sometimes', 'string', 'max:255'],
            'carrierName' => ['sometimes', 'string', 'max:255'],
            'fcmToken' => ['sometimes', 'string', 'max:255'],
        ];
    }

    /**
     * Custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'in' => 'The :attribute must be one of the following values: :values.',
        ];
    }
}
