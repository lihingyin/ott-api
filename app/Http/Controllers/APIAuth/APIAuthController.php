<?php

namespace App\Http\Controllers\APIAuth;

use App\Http\Requests\APIAuth\LoginRequest;
use App\Http\Requests\APIAuth\RegisterRequest;
use App\Http\Resources\UserTokenResource;
use App\Models\Device;
use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class APIAuthController extends BaseController
{
    public function login(LoginRequest $request)
    {
        $request->authenticate();

        $user = Auth::user();

        $device = Device::where('uuid', $request->uuid)->first();
        $device->user_id = $user->id;
        $device->save();

        //@todo: should we void previous token?
        // $user->tokens()->where('device_name', $request->input('device_uuid'))->delete();

        return new UserTokenResource($user);
    }

    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => $request->password,
        ]);

        $device = Device::where('uuid', $request->uuid)->first();
        $device->user_id = $user->id;
        $device->save();

        Auth::login($user);

        return new UserTokenResource($user);
    }

    public function logout()
    {
        Auth::user()->currentAccessToken()->delete();
        return ['result' => true];
    }

    public function info()
    {
        $user = Auth::user();
        return response()->json([
            "result" => true,
            "id" => $user->id,
            "name" => $user->name,
            "username" => $user->username,
        ]);
    }
}

