<?php

namespace App\Http\Controllers;

use App\Enums\FilePath;
use App\Helper\FileHelper;
use App\Http\Requests\Channels\ChannelListRequest;
use App\Models\Channel;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class ChannelController extends Controller
{
    private int $limit;
    private string $cacheTag = 'channel';
    private readonly string $cacheKeyForList, $cacheMinutes, $filePath;

    public function __construct()
    {
        $this->limit = config('app.list_item_default_limit');
        $this->cacheKeyForList = $this->cacheTag . '-list-type-%s-offset-%d-limit-%d-order-%s';
        $this->cacheMinutes = config('app.cache.minutes.channels');
        $this->filePath = FilePath::CHANNEL->value;
    }

    /**
     * Get Channel List
     *
     * @param ChannelListRequest $request
     * @return JsonResponse
     */
    public function list(ChannelListRequest $request): JsonResponse
    {
        $channelType = $request->input('type');
        $offset = $request->input('offset', 0);
        $order = $request->input('order', 'desc');
        $limit = $this->limit;
        $filePath = $this->filePath;

        $items = Cache::tags([$this->cacheTag])->remember(sprintf($this->cacheKeyForList, $channelType, $offset, $limit, $order), now()->addMinutes($this->cacheMinutes), static function () use ($channelType, $offset, $limit, $order, $filePath) {
            $items = [];
            $channels = (new Channel)
                ->when($channelType, fn($query) => $query->channelType($channelType))
                ->active()
                ->offset($offset)->limit($limit)
                ->orderBy('name', $order)
                ->get();

            foreach ($channels as $channel) {
                $items[] = [
                    'id' => $channel['id'],
                    'name' => $channel['name'],
                    'description' => $channel['description'],
                    'type' => $channel['type'],
                    'thumbnail' => FileHelper::getUrl($filePath, $channel['thumbnail']),
                ];
            }
            return $items;
        });

        return response()->json([
            'result' => true,
            'items' => $items,
        ]);
    }
}
