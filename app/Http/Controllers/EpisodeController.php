<?php

namespace App\Http\Controllers;

use App\Enums\FilePath;
use App\Exceptions\RecordNotFoundException;
use App\Helper\FileHelper;
use App\Http\Requests\Episodes\EpisodeListRequest;
use App\Models\Channel;
use App\Models\Episode;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class EpisodeController extends Controller
{
    private int $limit;
    private string $cacheTag = 'episode';
    private readonly string $cacheKeyForList, $cacheMinutes, $filePath;

    public function __construct()
    {
        $this->limit = config('app.list_item_default_limit');
        $this->cacheKeyForList = $this->cacheTag . '-list-channel-%d-offset-%d-limit-%d-order-%s';
        $this->cacheMinutes = config('app.cache.minutes.episodes');
        $this->filePath = FilePath::EPISODE->value;
    }

    /**
     * Get Episode List
     *
     * @param EpisodeListRequest $request
     * @return JsonResponse
     * @throws RecordNotFoundException
     */
    public function list(EpisodeListRequest $request): JsonResponse
    {
        $channelId = $request->route('channelId');
        $offset = $request->input('offset', 0);
        $order = $request->input('order', 'desc');
        $limit = $this->limit;
        $filePath = $this->filePath;

        $items = Cache::tags([$this->cacheTag])->remember(sprintf($this->cacheKeyForList, $channelId, $offset, $limit, $order), now()->addMinutes($this->cacheMinutes), static function () use ($channelId, $offset, $limit, $order, $filePath) {
            $items = [];
            $channel = Channel::find($channelId);
            if(empty($channel)) {
                throw new RecordNotFoundException();
            }
            $episodes = $channel->episodes()
                ->active()
                ->offset($offset)->limit($limit)
                ->orderBy('name', $order)
                ->get();
            foreach ($episodes as $episode) {
                $items[] = [
                    'id' => $episode['id'],
                    'name' => $episode['name'],
                    'description' => $episode['description'],
                    'thumbnail' => FileHelper::getUrl($filePath, $episode['thumbnail']),
                    'number' => $episode['number'],
                ];
            }
            return $items;
        });

        return response()->json([
            'result' => true,
            'items' => $items,
        ]);
    }
}
