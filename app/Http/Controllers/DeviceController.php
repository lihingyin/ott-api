<?php

namespace App\Http\Controllers;

use App\Enums\DevicePlatform;
use App\Http\Requests\RegisterDeviceRequest;
use App\Models\Device;
use Illuminate\Http\JsonResponse;

class DeviceController extends Controller
{
    public function registerDevice(RegisterDeviceRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $primaryDeviceId = match ($validated['platform']) {
            DevicePlatform::ANDROID->value => $validated['androidId'],
            DevicePlatform::IOS->value => $validated['primaryUUID'],
            DevicePlatform::BROWSER->value => $validated['uuid'],
        };

        $device = (new Device)
            ->where('uuid', $validated['uuid'])
            ->orWhere('primary_device_id', $primaryDeviceId)
            ->first();
        if (empty($device)) {
            $device = new Device;
            $device->primary_device_id = $primaryDeviceId;
        }

        $device->uuid = $validated['uuid'];
        $device->platform = $validated['platform'];
        if (!is_null($validated['osLanguage'])) {
            $device->os_language = $validated['osLanguage'];
        }
        if (!is_null($validated['appLanguage'])) {
            $device->app_language = $validated['appLanguage'];
        }
        if (!is_null($validated['osVersion'])) {
            $device->os_version = $validated['osVersion'];
        }
        if (!is_null($validated['appVersion'])) {
            $device->app_version = $validated['appVersion'];
        }
        if (!is_null($validated['buildVersion'])) {
            $device->build_version = $validated['buildVersion'];
        }
        if (!is_null($validated['appBundleId'])) {
            $device->app_bundle_id = $validated['appBundleId'];
        }
        if (!is_null($validated['deviceName'])) {
            $device->device_name = $validated['deviceName'];
        }
        if (!is_null($validated['deviceBrand'])) {
            $device->device_brand = $validated['deviceBrand'];
        }
        if (!is_null($validated['deviceModel'])) {
            $device->device_model = $validated['deviceModel'];
        }
        if (!is_null($validated['carrierName'])) {
            $device->carrier_name = $validated['carrierName'];
        }
        if (!is_null($validated['fcmToken'])) {
            $device->fcm_token = $validated['fcmToken'];
        }
        $device->save();

        return response()->json([
            'result' => true,
        ]);
    }
}
