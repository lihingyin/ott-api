<?php

namespace App\Http\Controllers;

use App\Helper\SystemHelper;
use Illuminate\Http\Request;
use App\Models\ApiAccessLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LogController extends Controller
{
    public static function apiAccessLog(Request $request, $response): void
    {
        if (config("app.access_log_in_database_enable")) {
            $apiAccessLog = new ApiAccessLog;
            $apiAccessLog->path = $request->path();

            $userIp = SystemHelper::clientIp($request);
            $apiAccessLog->ip_address = $userIp;

            $apiAccessLog->request_method = $request->getMethod();

            $requestBody = $request->input();
            $apiAccessLog->request_body = json_encode($requestBody, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

            $requestHeader = $request->header();
            $apiAccessLog->request_header = json_encode($requestHeader, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

            // Response (Output)
            $responseBody = $response->getContent();
            $apiAccessLog->response_body = $responseBody;

            $apiAccessLog->save();
        }

        if (config("app.access_log_in_stdout_enable")) {
            Log::info("API Access", [
                "PATH" => $request->path(),
                "IP" => $userIp,
                "REQUEST_BODY" => $requestBody,
                "REQUEST_HEADER" => $requestHeader,
                "RESPONSE_BODY" => $responseBody,

                "USER_ID" => Auth::id(),
                "TOKEN" => request()->bearerToken(),
                "SESSION_ID" => session()->getId(),
            ]);
        }
    }
}
