<?php

namespace App\Http\Controllers;

use App\Enums\FilePath;
use App\Exceptions\RecordNotFoundException;
use App\Helper\FileHelper;
use App\Models\Channel;
use App\Models\Episode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class StreamingController extends Controller
{
    private string $cacheTag = 'streaming-info';
    private readonly string $liveInfoCacheKey, $linearInfoCacheKey, $cacheMinutes;

    public function __construct()
    {
        $this->liveInfoCacheKey = $this->cacheTag . '-channel-%d';
        $this->linearInfoCacheKey = $this->cacheTag . '-channel-%d-episode-%d';
        $this->cacheMinutes = config('app.cache.minutes.streaming_info');
    }

    /**
     * Get Linear Streaming Info
     *
     * @param Request $request
     * @return JsonResponse
     * @throws RecordNotFoundException
     */
    public function getLiveInfo(Request $request): JsonResponse
    {
        $channelId = $request->route('channelId');
        $filePath = FilePath::CHANNEL->value;

        $linearStreamingInfo = Cache::tags([$this->cacheTag])->remember(sprintf($this->liveInfoCacheKey, $channelId), now()->addMinutes($this->cacheMinutes), static function () use ($channelId, $filePath) {
            $channel = Channel::find($channelId);
            if(empty($channel)) {
                throw new RecordNotFoundException();
            }
            $liveStreamingInfo = $channel->liveStreamingInfo()->first();
            if(empty($liveStreamingInfo)) {
                throw new RecordNotFoundException();
            }

            return [
                'result' => true,
                'channelName' => $channel['name'],
                'thumbnail' => FileHelper::getUrl($filePath, $channel['thumbnail']),
                'streamingUrl' => $liveStreamingInfo['streaming_url'],
                'deliveryType' => $liveStreamingInfo['delivery_type'],
            ];
        });

        return response()->json($linearStreamingInfo);
    }

    /**
     * Get Linear Streaming Info
     *
     * @param Request $request
     * @return JsonResponse
     * @throws RecordNotFoundException
     */
    public function getLinearInfo(Request $request): JsonResponse
    {
        $channelId = $request->route('channelId');
        $episodeId = $request->route('episodeId');
        $filePath = FilePath::EPISODE->value;

        $linearStreamingInfo = Cache::tags([$this->cacheTag])->remember(sprintf($this->linearInfoCacheKey, $channelId, $episodeId), now()->addMinutes($this->cacheMinutes), static function () use ($channelId, $episodeId, $filePath) {
            $channel = Channel::find($channelId);
            if(empty($channel)) {
                throw new RecordNotFoundException();
            }
            $episode = Episode::find($episodeId);
            if(empty($episode)) {
                throw new RecordNotFoundException();
            }

            return [
                'result' => true,
                'channelName' => $channel['name'],
                'episodeName' => $episode['name'],
                'thumbnail' => FileHelper::getUrl($filePath, $episode['thumbnail']),
                'videoId' => $episode['video_id'],
                'number' => $episode['number'],
            ];
        });

        return response()->json($linearStreamingInfo);
    }
}
