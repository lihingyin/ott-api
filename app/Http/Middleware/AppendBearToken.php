<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AppendBearToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->input('token') && is_null($request->BearerToken())) {
            $request->headers->set('Authorization', 'Bearer ' . $request->input('token'));
        }
        return $next($request);
    }
}
