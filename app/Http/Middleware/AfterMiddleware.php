<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\LogController;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if(json_decode($response->getContent())!==null) {
            LogController::apiAccessLog($request,$response);
        }

        return $response;
    }
}
