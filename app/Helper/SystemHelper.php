<?php

namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Http\Request;

class SystemHelper
{
    /**
     * Return client IPv4
     *
     * @param Request|null $request
     * @return string|null
     */
    public static function clientIp(?Request $request = null) : string
    {
         $newRequest = $request ?? request();
         $ips = $newRequest->header('x-forwarded-for');
         foreach(explode(',', $ips) as $ip) {
             $ip = trim($ip);
             if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                 return $ip;
             }
         }

        return $newRequest->ip();
    }

    /**
     * Timestamp to date time using our format
     *
     * @param int $timestamp
     * @return string
     */
    public static function timestampToDateTime(int $timestamp) : string
    {
        return Carbon::createFromTimestampMs($timestamp)->format('Y-m-d H:i:s');
    }

    /**
     * Convert date time to general format
     *
     * @param Carbon|string $dateTime
     * @param string|null $tz
     * @return int
     */
    public static function dateTimeFormat(Carbon|string $dateTime, string $tz = null) : int
    {
        return Carbon::parse($dateTime, $tz)->getTimestampMs();
    }
}
