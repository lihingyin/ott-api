<?php

namespace App\Helper;

use Illuminate\Support\Facades\Storage;

class FileHelper
{
    /**
     * Get file URL
     * @param string $filePath
     * @param string $fileName
     * @param string|null $disk
     * @return string
     */
    public static function getUrl(
        string $filePath,
        string $fileName,
        string $disk = null,
    ): string
    {
        $disk ??= config('filesystems.default');
        return Storage::disk($disk)->url($filePath.$fileName);
    }

}
