<?php

namespace App\Enums;

enum FilePath: string
{
    case CHANNEL = 'channel/';
    case EPISODE = 'episode/';
}
