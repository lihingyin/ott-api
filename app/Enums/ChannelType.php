<?php

namespace App\Enums;

enum ChannelType: string
{
    case LIVE = 'live';
    case LINEAR = 'linear';
}
