<?php

namespace App\Enums;

enum DevicePlatform: string
{
    case ANDROID = 'android';
    case IOS = 'ios';
    case BROWSER = 'browser';
}
