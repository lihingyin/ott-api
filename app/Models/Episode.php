<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Episode extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $table = 'episodes';

    public function scopeActive($query){
        return $query->where('active', true);
    }
}
