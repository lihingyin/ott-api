<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Channel extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $table = 'channels';

    public function scopeChannelType($query, string $channelType)
    {
        return $query->where('type', $channelType);
    }

    public function scopeActive($query){
        return $query->where('active', true);
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class, 'channel_id');
    }

    public function liveStreamingInfo()
    {
        return $this->hasOne(LiveStreamingInfo::class, 'channel_id');
    }
}
