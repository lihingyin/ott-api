<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class CustomAuthenticationException extends Exception
{
    public int $httpStatusCode;
    public ?string $bodyCode;

    public function __construct()
    {
        $this->httpStatusCode = Response::HTTP_UNAUTHORIZED;
        $this->bodyCode = config('error.user.unauthenticated');
        $this->message = __(config('error.user.unauthenticated'));
        parent::__construct($this->message);
    }

    public function render()
    {
        return response()->json([
            'result' => false,
            'error' => [
                'message' => $this->message,
                'code' => $this->bodyCode,
            ]
        ], $this->httpStatusCode);
    }
}
