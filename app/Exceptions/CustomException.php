<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class CustomException extends Exception
{
    public int $httpStatusCode;
    public string $bodyCode;

    public function __construct(int $httpStatusCode = null, $bodyCode = null, $message = null)
    {
        $this->httpStatusCode = $httpStatusCode ?? Response::HTTP_INTERNAL_SERVER_ERROR;
        $this->bodyCode = $bodyCode ?? config('error.general.try_again');
        $this->message = $message ?? __(config('error.general.try_again'));
        parent::__construct($message);
    }

    public function render()
    {
        return response()->json([
            'result' => false,
            'error' => [
                'message' => $this->message,
                'code' => $this->bodyCode,
            ]
        ], $this->httpStatusCode);
    }
}
