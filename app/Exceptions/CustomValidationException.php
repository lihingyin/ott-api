<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class CustomValidationException extends Exception
{
    public int $httpStatusCode;
    public ?string $bodyCode;
    public $message;
    public ?array $errors;

    public function __construct(?array $errors)
    {
        $this->httpStatusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
        $this->bodyCode = config('error.general.invalid_input');
        $this->message = __(config('error.general.invalid_input'));
        $this->errors = $errors;
        parent::__construct($this->message);
    }

    public function render()
    {
        return response()->json([
            'result' => false,
            'error' => [
                'message' => $this->message,
                'code' => $this->bodyCode,
                'fields' => $this->errors,
            ],
        ], $this->httpStatusCode);
    }
}
