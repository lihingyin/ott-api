<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class CustomNotFoundHttpException extends Exception
{
    public int $httpStatusCode;
    public ?string $bodyCode;

    public function __construct()
    {
        $this->httpStatusCode = Response::HTTP_NOT_FOUND;
        $this->bodyCode = config('error.network.not_found');
        $this->message = __(config('error.network.not_found'));
        parent::__construct($this->message);
    }

    public function render()
    {
        return response()->json([
            'result' => false,
            'error' => [
                'message' => $this->message,
                'code' => $this->bodyCode,
            ]
        ], $this->httpStatusCode);
    }
}
