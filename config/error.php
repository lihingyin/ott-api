<?php

declare(strict_types=1);

return [
    'general' => [
        'try_again' => 'error.general.try_again',
        'try_again_message' => 'Something went wrong. Please try again.',

        'invalid_input' => 'error.general.invalid_input',
        'invalid_input_message' => 'The given data was invalid.',
    ],
    'network' => [
        'method_not_allowed' => 'error.network.method_not_allowed',
        'method_not_allowed_message' => 'The method is not supported for this route.',

        'not_found' => 'error.network.not_found',
        'not_found_message' => 'Page not found.',
    ],
    'user' => [
        'unauthorized' => 'error.user.unauthorized',
        'unauthorized_message' => 'This action is unauthorized.',

        'unauthenticated' => 'error.user.unauthenticated',
        'unauthenticated_message' => 'Unauthenticated.',
    ],
    'not_found' => [
        'record' => 'error.not_found.record',
        'record_message' => 'Record not found.',
    ],
    'device' => [
        'fcm_not_registered' => 'error.device.fcm_not_registered',
        'fcm_not_registered_message' => 'Device not registered for FCM',
    ],
];
